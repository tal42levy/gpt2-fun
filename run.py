import sys
from os import path
import gpt_2_simple as gpt2

checkpoint_dir = sys.argv[1]
sess = gpt2.start_tf_sess()
gpt2.load_gpt2(sess, checkpoint_dir=checkpoint_dir)
text = gpt2.generate(
    sess,
    checkpoint_dir=checkpoint_dir,
    length=500,
    temperature=1,
    nsamples=100,
    destination_path=None,
    prefix="<|startoftext|>",
    include_prefix=False,
    truncate="<|endoftext|>",
    # return_as_list=True,
)
